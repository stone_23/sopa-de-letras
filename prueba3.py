import arcade

# Dimensiones de la ventana
ANCHO_VENTANA = 600
ALTO_VENTANA = 600

# Dimensiones de la cuadrícula de la sopa de letras
ANCHO_CUADRICULA = 10
ALTO_CUADRICULA = 10    

# Tamaño de cada celda de la cuadrícula
ANCHO_CELDA = ANCHO_VENTANA / ANCHO_CUADRICULA
ALTO_CELDA = ALTO_VENTANA / ALTO_CUADRICULA

# Definición de la cuadrícula de la sopa de letras
cuadricula = [
    ["x","m","ñ","a","v","x","a","w","m","e"],
    ["g","w","l","a","g","t","s","a","y","u"],
    ["m","e","ñ","l","l","a","v","e","s","a"],
    ["o","m","k","e","e","o","l","a","a","r"],
    ["v","o","d","a","h","o","l","a","n","a"],
    ["m","a","m","o","c","o","x","a","a","i"],
    ["a","h","o","d","o","o","l","a","ñ","d"],
    ["o","c","k","e","n","m","p","s","a","p"],
    ["ñ","m","l","x","m","e","s","a","m","e"],
    ["s","o","l","a","c","e","h","d","f","e"]
]

def dibujar_cuadricula():
    for fila in range(ALTO_CUADRICULA):
        for columna in range(ANCHO_CUADRICULA):
            x = columna * ANCHO_CELDA
            y = fila * ALTO_CELDA
            letra = cuadricula[fila][columna]
            arcade.draw_text(letra, x + ANCHO_CELDA/2, y + ALTO_CELDA/2, arcade.color.WHITE, font_size=20, anchor_x='center', anchor_y='center')
            arcade.draw_rectangle_outline(x+ANCHO_CELDA/2, y + ALTO_CELDA/2,60,60,arcade.color.SKY_BLUE,5)
    arcade.draw_text("AYAYATAYTA", 350 ,+700, arcade.color.ARMY_GREEN, font_size=75, font_name="Kenney High Square",anchor_x='center', anchor_y='center')        
    arcade.draw_text("sol", 675 ,550, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')       
    arcade.draw_text("dia", 675 ,500, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
    arcade.draw_text("noche", 675 ,450, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')  
    arcade.draw_text("llaves", 675 ,400, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
    arcade.draw_text("mesa", 675 ,350, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
    arcade.draw_text("mañana", 675 ,300, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
    arcade.draw_text("hola", 675 ,250, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
    arcade.draw_text("chao", 675 ,200, arcade.color.AMAZON, font_size=25,font_name="Kenney High Square" , anchor_x='center', anchor_y='center')
def dibujar_sopa_de_letras(delta_time):
    arcade.start_render()
    dibujar_cuadricula()
  
   

def main():
    arcade.open_window(ANCHO_VENTANA+150, ALTO_VENTANA+150, "Sopa de Letras")
  #  arcade.open_window(ANCHO_VENTANA+150, ALTO_VENTANA+150, "sopa2")
    arcade.set_background_color(arcade.color.BLACK)
    arcade.schedule(dibujar_sopa_de_letras, 1/60)
    arcade.run()
    

if __name__ == "__main__":
    main()
    